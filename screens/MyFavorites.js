import React, {Component } from 'react';
import { View, Text, ScrollView, StyleSheet, FlatList } from 'react-native';
import Title from '../components/Title';
import {connect} from "react-redux";
import { updateFavoris } from "../actions/favoris.actions";
import RecipeServices from "../services/RecipeServices";
import RecipeBox from "../components/RecipeBox";
import { updateUserId } from "../actions/login.actions";

class MyFavorites extends Component{
    constructor(props) {
        super(props);
        this.state = {
            recipes: [],
            userId: null
        }
    }
    async componentDidMount() {
        let response = await RecipeServices.listAll();
        if(response.ok){
            let data = await response.json();
            this.setState({recipes: data.recipe});
            // console.log('bien reçu', data);
            // data.recipes.map(i => {
            //     console.log(i.id);
            //
            // });
            // console.log(recipes);

        }
        // console.log('entrée', this.props, this.state);
        // let {data} = this.state;
        // favoris.includes(data) ? '' : '';
    }


    render() {
        let {recipes} = this.state;
        let {favoris} = this.props;
        let { navigation } = this.props;

        return(
            <ScrollView style={styles.container}>
                <Title title="mes favoris"/>
                {recipes.map((item) => {
                    // console.log(favoris.includes(item._id));
                    return(
                    favoris.includes(item._id) ? <RecipeBox data={item} navigation={navigation} key={item._id}/>: null)
                })}


            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
   container: {
        flex: 1,
        backgroundColor: "#e3f1ff",
        paddingTop: 70
    }
});
const mapStateToProps = state => {
    return {
        favoris: state.favoris,
        userId: state.userId
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateFavoris: favoris => {dispatch(updateFavoris(favoris))},
        updateUserId: userId => {dispatch(updateUserId(userId))}
    }
};



export default connect(mapStateToProps, mapDispatchToProps)(MyFavorites);