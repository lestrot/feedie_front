import React, {Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    ImageBackground,
    Button,
    TouchableOpacity,
    StyleSheet,
    Linking
} from 'react-native';
import Title from "../components/Title";
import {updateFavoris} from "../actions/favoris.actions";
import {connect} from 'react-redux';
import recipeimg from '../assets/recipes/recipe.jpg';
import RecipeServices from "../services/RecipeServices";
import Icon from 'react-native-vector-icons/FontAwesome';
import Helpers from "../helpers/Helpers";




class Details extends Component{

    constructor(props) {
        super(props);
        this.state = {
            data: this.props.navigation.state.params.data,
            short_description: null,
            description: null,
            isFavoris: false,
            userId: null
        }
    }

    componentDidMount() {
        let {description} = this.state.data;
        let short_description = description.replace(/(<([^>]+)>)/ig, '').substr(0,300) + "... en voir plus";
        description = description.replace(/(<([^>]+)>)/ig, '');

        let isFavoris = false;
        let {favoris} = this.props;
        let {data} = this.state;
        console.log(favoris, data);

        favoris.includes(data._id) ? isFavoris = true : isFavoris = false;
        this.setState({short_description, description, isFavoris});
    }

    openDescription(){
        this.setState({short_description: this.state.description});
    }



    addOrDeleteFavoris(){
        let {favoris} = this.props;
        let {_id} = this.state.data;
        let body = {
            recipeId: _id,
            userId: 'toDetermineAfterImplementation'
        };
        console.log(body);

        favoris.includes(_id) ? favoris.splice(favoris.indexOf(_id), 1) : favoris.push(_id);
        // favoris.includes(_id) ? RecipeController.addFavorite(_id, ) : null ;
        favoris.includes(_id) ? RecipeServices.addFavorite(body) : null;
        this.props.updateFavoris(favoris);
        console.log(this.props.favoris);
        this.setState({ isFavoris : !!favoris.includes(_id) })
    }
    goBack(){
        this.props.navigation.navigate('Home');
    }
     addFavoris(){
        let {favoris} = this.props;
        let {_id} = this.state.data;

        if(!favoris.includes(_id)){
            favoris.push(_id);
            this.props.updateFavoris(favoris);
            this.setState({isFavoris: true});
                    console.log(this.props.favoris);
        }
    }

    deleteFavoris(){
        let {favoris} = this.props;
        let {_id} = this.state.data;

        favoris.splice(favoris.indexOf(_id), 1);
        this.props.updateFavoris(favoris);
        this.setState({isFavoris: false});
                console.log(this.props.favoris);
    }

    async deleteRecipe(){
        let {_id} = this.props;
        let response = await RecipeServices.delete(_id);
        response.ok ? this.props.navigate.navigate('Home') : null;
    }


    render() {

        let {_id, name, visual, description, preparation_time, creation_date,  like_numbers, category, firstname, recipes, avatar, creator} = this.state.data;
        let {short_description, isFavoris, userId} = this.state;


        return (
            <ScrollView style={styles.container}>
                <TouchableOpacity style={styles.back} onPress={() => this.goBack()}>
                    <Icon name={"arrow-left"} size={30} color={'#122130'}/>
                </TouchableOpacity>
                <ImageBackground style={styles.headerImage} source={{uri: recipeimg}}/>

                <View style={styles.body}>
                    {/*<Title title={name}/>*/}

                    <View>
                        <Text>{avatar}</Text>
                        <Text>{firstname}</Text>
                        {isFavoris ?
                            <TouchableOpacity style={styles.button2} onPress={() => this.addOrDeleteFavoris()}>
                                <Icon name={"heart-o"} size={30} color={'#122130'}/>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.button} onPress={() => this.addOrDeleteFavoris()}>
                                <Icon name={"heart"} size={30} color={'#122130'}/>
                            </TouchableOpacity>}
                        {userId === creator ?
                            <TouchableOpacity style={styles.button3} onPress={() => this.deleteRecipe()}>
                                <Text>Supprimer des favoris</Text>
                            </TouchableOpacity> : null}
                    </View>

                    {/*Box icon*/}

                    <View>
                        <Text style={styles.title}>{name}</Text>
                        <Text style={styles.isolate}>{category}</Text>
                        <Text>{Helpers.extractDayFromDate(creation_date)}/{Helpers.extractMonthFromDate(creation_date)}/2020</Text>
                        <Text>{like_numbers}</Text>
                    </View>

                    <View>
                        <Text><Icon name={"hourglass"} size={13} color={'#717575'}/> {preparation_time} mins</Text>
                        {/*{recipes.map(item => {*/}
                        {/*    return (<Text>{item}</Text>)*/}
                        {/*})}*/}
                    </View>


                    <Text>{description}</Text>



                </View>

            </ScrollView>
        );
    }


}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    headerImage: {
        height: 300
    },
    body: {
        // marginRight: 13,
        display: "flex",
        justifyContent: "center",
        height: 150,
        backgroundColor: "#FFF",
        borderTopStartRadius: 30,
        borderTopEndRadius: 30,
        width: '100%',
        paddingLeft: 35,
        marginTop: -30,
        // elevation: 5,
        // position: "relative",
    },
    isolate: {
        fontSize: 18,
        fontWeight: "600",
        color: '#33d87f',
        marginBottom: 17,
    },
    back: {
        position: 'absolute',
        top: 10,
        left: 10,
        zIndex: 999
    },
    button: {
        // alignSelf: 'flex-start',
        // color: 'red',
        backgroundColor: '#33d87f',
        margin: 20,
        padding: 20,
        borderRadius: 40,
        position: 'absolute',
        zIndex: 999,
        right: 20,
        bottom: -250,
    },
    button2: {
        // alignSelf: 'flex-start',
        // color: 'red',
        backgroundColor: '#d86561',
        margin: 20,
        padding: 20,
        borderRadius: 40,
        position: 'absolute',
        zIndex: 999,
        right: 20,
        bottom: -250,
    },
    button3: {
        // alignSelf: 'flex-start',
        // color: 'red',
        backgroundColor: '#d86561',
        margin: 20,
        padding: 20,
        borderRadius: 20,
    },
    title: {
        marginTop: 15,
        fontSize: 38,
        fontWeight: "800",
        marginBottom: 20,
    },
});

const mapStateToProps = state => {
    return {
        favoris: state.favoris
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateFavoris: favoris => {dispatch(updateFavoris(favoris))},
    }
};



export default connect(mapStateToProps, mapDispatchToProps)(Details);
