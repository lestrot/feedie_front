import React, {Component} from 'react';
import { StyleSheet, View, Animated, TextInput, Button, TouchableOpacity, Text, Image} from 'react-native';
import tcomb from 'tcomb-form-native';
import UserServices from "../services/UserServices";
import {connect} from 'react-redux';
import { updateUserId } from "../actions/login.actions";
import splash2 from './../assets/logginlogo2.png';

// initialisation
const Form = tcomb.form.Form;

// model
const LoginModel = tcomb.struct({
    email: tcomb.String,
    password: tcomb.String
});
const RegisterModel = tcomb.struct({
    name: tcomb.String,
    lastname: tcomb.String,
    email: tcomb.String,
    password: tcomb.String
});


class Login extends Component{
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
            registerForm: null,
            name: null,
            lastname: null,
        };

        let inputRef = React.createRef();
    }

    async connect(){
        let validate = this.refs.form.validate();
        this.props.navigation.navigate('Home');
        let {email, password} = this.state;
        let {userId} = this.props;
        if (validate.value.email !== null && validate.value.password !== null){
            let body = {
                email: validate.value.email,
                password: validate.value.password
            };
            let response = await UserServices.login(body);
            userId.push('toDetermineLater');
            this.props.updateUserId(userId);
            response.ok ? this.props.navigation.navigate('Home'): null;
        }
    }

    async register(){
        let validate = this.res.form.validate();
        let {userId} = this.props;
        if (validate.value.email !== null && validate.value.password !== null){
            let body = {
                firstname: validate.value.email,
                lastname: validate.value.lastname,
                email: validate.value.email,
                password: validate.value.password,
                avatar: validate.value.avatar
            };
            userId.push('toImplementLater');
            this.props.updateUserId(userId);
            let response = await UserServices.create(body);
            response.ok ? this.switchForm() : null;
        }
    }

    handleChangeEmail(text){
        this.setState({
            email: text
        })
    }
    handleChangePassword(text){
        this.setState({
            password: text
        })
    }

    handleChange(e, name){
        this.setState({
        [name]: e.nativeEvent.text
        });
        // [e.target.id]: e.target.value
    }
    switchForm(){
        this.setState({
            registerForm: true
        })
    }
    switchBack(){
        this.setState({
            registerForm: false
        })
    }


    componentDidMount() {
        // this.containerRef.addEventListener("keypress")
    }

    render() {
        let { registerForm } = this.state;
        return (
            <View style={styles.container}>
                {/*<TextInput placeholder={'Entrez votre email.'}*/}
                {/*           // onChangeText={(e) => this.handleChangeEmail(e,'email')}*/}
                {/*           onChange={(e) => this.handleChange(e, "email")}*/}
                {/*           id={"email"}*/}
                {/*            name={""}/>*/}
                {/*<TextInput*/}
                {/*    placeholder={'Entrez votre mot de passe'}*/}
                {/*    secureTextEntry={true}*/}
                {/*    // onChangeText={(text) => this.handleChangePassword(text)}*/}
                {/*    onChange={(e) => this.handleChange(e, "password")}/>*/}
                <Image source={splash2} style={styles.image} resizeMode={"cover"}/>
                {!registerForm ? <Form ref="form" type={LoginModel} value={this.state} style={styles.form}/> :
                <Form ref="form" type={RegisterModel} value={this.state} style={styles.form}/>}

                {/*{!registerForm ? <Form ref={c => this._form = c} type={LoginModel} value={this.state} /> :*/}
                {/*<Form ref={c => this._form = c} type={RegisterModel}  value={this.state} />}*/}

                <View style={styles.buttonBox}>
                    {!registerForm ?
                        <TouchableOpacity style={styles.buttons} onPress={() => this.connect()}>
                            <Text style={styles.text}>Se connecter</Text>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity style={styles.buttons} onPress={() => this.switchForm()}>
                            <Text style={styles.text}>Valider l'inscription</Text>
                        </TouchableOpacity>
                    }
                    {/*<Button style={styles.buttons} title={"Valider l'inscription"} onPress={() => this.switchForm()}/>*/}
                    {!registerForm ?
                        // <Button style={styles.buttons} title={"S'inscrire"} onPress={() => this.switchForm()}/>
                        <TouchableOpacity style={styles.buttonsRegister} onPress={() => this.switchForm()}>
                            <Text style={styles.text}>S'inscrire</Text>
                        </TouchableOpacity>
                    :
                        // <Button style={styles.buttons} title={"revenir"} onPress={() => this.switchBack()}/>
                        <TouchableOpacity style={styles.buttonsBack} onPress={() => this.switchBack()}>
                            <Text style={styles.text}>Revenir</Text>
                        </TouchableOpacity>}

                </View>


                {/* les deux font la meme, mais il est plus facile de styliser le touchable opacity*/}
                {/* button englobe des fonctions qui le rendent plus compliqué a changer son visuel*/}
                {/*<TouchableOpacity onPress={() => this.connect()}>*/}
                {/*    <Text>Se connecter</Text>*/}
                {/*</TouchableOpacity>*/}
                {/*<TouchableOpacity onPress={() => this.switchForm()}>*/}
                {/*    <Text>S'inscrire</Text>*/}
                {/*</TouchableOpacity>*/}
            </View>
        )
    }


}
const mapStateToProps = state => {
    return {
        userId: state.userId
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateUserId: userId => {dispatch(updateUserId(userId))},
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);


const styles = StyleSheet.create({

    container: {
        display: 'flex',
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        flexDirection: 'column',
        backgroundColor: "#00ae8f"
    },
    input: {
        width: '100%',
        fontSize: 20,
        marginBottom: 20,
        marginLeft:20,
    },
    image: {
        marginTop: 50,
        alignSelf: 'center',
        // maxWidth: '20rem',
        // minWidth: '100px',
        // display: 'none'
    },
    buttons: {
        alignSelf: 'flex-start',
        color: 'red',
        backgroundColor: '#fea4a6',
        margin: 10,
        padding: 10,
        borderRadius: 20,
    },
    buttonsRegister:{
        backgroundColor: '#fac932',
        alignSelf: 'flex-start',
        color: 'red',
        margin: 10,
        padding: 10,
        borderRadius: 20,
    },
    buttonsBack:{
        backgroundColor: '#fac932',
        alignSelf: 'flex-start',
        color: 'red',
        margin: 10,
        padding: 10,
        borderRadius: 20,
    },
    form: {
        backgroundColor: '#FFF',
        color: '#FFF',
    },
    text: {
        color: '#FFF',
    },
    buttonBox:{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around'
    }

});
