import React, {Component} from 'react';
import { StyleSheet, View, Animated, Text, ScrollView } from 'react-native';
import splash2 from './../assets/splashscreen2.png';

class Splash extends Component{
    constructor(props) {
        super(props);
        this.state = {
            fadeIn: new Animated.Value(0)
        }
    }

    fadeInAnimation = () => {
        let {fadeIn} = this.state;
        Animated.timing(fadeIn, {
            toValue: 1,
            duration: 2000,
        }).start(() => this.props.navigation.navigate('Login'));
    };

    componentDidMount() {
        this.fadeInAnimation();
    }


    render() {

        let {fadeIn} = this.state;

        return (
            <View style={styles.container}>
                <Animated.Image
                    // style={{opacity: fadeIn, style:styles.headerImage}}
                    style={styles.headerImage}
                    source={splash2}
                />
            </View>
        )
    }


}

export default Splash;


const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 0,
        padding: 0,
        backgroundColor: '#00ae8f',
    },
    headerImage: {
        marginTop: 0,
        paddingTop: 0,
        flex: 1,
        justifyContent: 'flex-start',
        resizeMode: 'contain',
    },

});
