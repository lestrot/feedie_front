import React, {Component } from 'react';
import { View, Text, ScrollView, StyleSheet, Button, FlatList, TouchableOpacity } from 'react-native';
import Title from '../components/Title';
import {connect} from "react-redux";
import RecipeServices from "../services/RecipeServices";
import RecipeBox from "../components/RecipeBox";
import { updateUserId } from "../actions/login.actions";
import Icon from 'react-native-vector-icons/FontAwesome';


class MyRecipes extends Component{
    constructor(props) {
        super(props);
        this.state = {
            recipes: [],
            userId: null
        }
    }

    async componentDidMount() {
        let {userId} = this.props;
        console.log('userId ici', userId);
        console.log('props ici', this.props);
        let response = await RecipeServices.listAll();
        if(response.ok) {
            let data = await response.json();
            await this.setState({ recipes: data.recipe });
        }
        this.props.updateUserId(userId);
        this.setState({userId: '5f046de1bdb2bb4928b95c68'});
    }

    goToNewRecipe(){
        this.props.navigation.navigate('NewRecipe');
    }

    render() {
        let {recipes} = this.state;
        let {userId} =this.props;
        console.log('ton pre', this.state);
        return(
            <ScrollView style={styles.container}>
                <Title title="mes plats"/>
                <TouchableOpacity style={styles.buttons} onPress={() => this.goToNewRecipe()}>
                    <Text style={styles.text}>+</Text>
                </TouchableOpacity>
                {/*{recipes.map((item) => {*/}
                {/*    // console.log(favoris.includes(item._id));*/}
                {/*    return(*/}
                {/*    userId.includes(item.creator) ? <FlatList*/}
                {/*        data={item}*/}
                {/*        showHorizontalScrollIndicator={false}*/}
                {/*        backgroundColor={"#FFF"}*/}
                {/*        keyExtractor={item._id}*/}
                {/*        key={item._id}*/}
                {/*        renderItem={(item) => <RecipeBox data={item} navigation={navigation}/>}*/}
                {/*    />: null)*/}
                {/*})}*/}

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#e3f1ff",
        paddingTop: 70,
    },
    buttons: {
        alignSelf: 'flex-end',
        // color: 'red',
        backgroundColor: '#fec739',
        // margin: 10,
        // padding: 10,
        borderRadius: 40,
        padding: 3,
        width: 80,
        height: 80,
        // position: 'absolute',
        // top: 10,
        // left: 10,
        zIndex: 99999,
        marginRight: 10,
    },
    text: {
        color: '#FFF',
        position: 'absolute',
        left: 30,
        top: 14,
        fontSize: 40,
    }
});

const mapStateToProps = state => {
    return {
        userId: state.userId
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateUserId: userId => {dispatch(updateUserId(userId))}
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MyRecipes);