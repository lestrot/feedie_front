import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    FlatList,
    Image
} from 'react-native';
import RecipeBox from "../components/RecipeBox";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import RecipeServices from "../services/RecipeServices";

class Search extends Component{

    constructor(props) {
        super(props);
        this.state = {
            recipes: []
        }
    }

    async searchRecipe(text){
        if(text.length > 2){
            let recipes = await RecipeServices.search(text);
            this.setState({recipes});
        }else{
            this.setState({recipes: []});
        }
    }

    render() {

        let {recipes} = this.state;
        let {navigation} = this.props;

        return (
            <View style={styles.container}>

                <View style={styles.header}>
                    {/*<FontAwesome></FontAwesome>*/}
                    {/*<FontAwesomeIcon className="infos" icon={ faSearch } color="#1833ffff"/>*/}
                    <Image source={faSearch} style={styles.headerImage}/>
                    <TextInput style={styles.inputSearch}
                        placeholder={"Rechercher un plat..."}
                        onChangeText={(text) => this.searchRecipe(text)}/>
                </View>

                <FlatList
                    data={recipes}
                    backgroundColor={"#FFF"}
                    keyExtractor={item => item.fields.id}
                    renderItem={({item}) => <RecipeBox data={item.fields} navigation={navigation}/>}
                />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#e3f1ff",
        flex: 1
    },
    header: {
        display: "flex",
        flexDirection: "row",
        marginTop: 70,
        alignItems: "center",
        marginHorizontal: 10,
        marginBottom: 20
    },
    headerImage:{
        marginRight: 10,
        width: 25,
        height: 25
    },
    inputSearch: {
        fontSize: 22
    }
});

export default Search;
