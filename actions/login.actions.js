export const updateUserId = (userId) => {
    return {
        type: "UPDATE_USERID",
        userId: userId
    }
};
