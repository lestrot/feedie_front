const baseUrl = "http://localhost:3010";

class RecipeServices {
    /**
     * function to get all users from server DB
     * @returns {Promise<Response>}
     */
    static async listAll()
    {
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        };

        let call = await fetch(`${baseUrl}/recipes`, init);
        return call;
    }
    /**
     * function to get all users from server DB
     * @returns {Promise<Response>}
     */
    static async search(body)
    {
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body)
        };

        let call = await fetch(`${baseUrl}/search`, init);
        return call;
    }
    /**
     * function to get all users from server DB
     * @returns {Promise<Response>}
     */
    static async listFavorites(id)
    {
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        };

        let call = await fetch(`${baseUrl}/favorites/${id}`, init);
        return call;
    }
    /**
     * function to get all users from server DB
     * @returns {Promise<Response>}
     */
    static async create(body)
    {
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body)
        };

        let call = await fetch(`${baseUrl}/recipes`, init);
        return call;
    }
    /**
     * function to add a recipe to favorite
     * @returns {Promise<Response>}
     */
    static async addFavorite(body)
    {
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body)
        };

        let call = await fetch(`${baseUrl}/favorites`, init);
        // console.log(call);
        return call;
    }
    /**
     * function to add a recipe to favorite
     * @returns {Promise<Response>}
     */
    static async deleteFavorite(body)
    {
        let init = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body)
        };

        let call = await fetch(`${baseUrl}/favorites`, init);
        return call;
    }
    /**
     * function to get all users from server DB
     * @returns {Promise<Response>}
     */
    static async update(id, body)
    {
        let init = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body)
        };

        let call = await fetch(`${baseUrl}/recipes/${id}`, init);
        return call;
    }
    /**
     * function to get all users from server DB
     * @returns {Promise<Response>}
     */
    static async delete(id)
    {
        let init = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        };

        let call = await fetch(`${baseUrl}/recipes/${id}`, init);
        return call;
    }
}

export default RecipeServices;