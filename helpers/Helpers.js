import moment from "moment";

class Helpers{
    static extractDayFromDate(date){
        return moment(date).format('DD');
    }

    static extractMonthFromDate(date){
        return moment(date).format('MM');
    }

    /**
     * Expositions-> art contemporain
     */
}

export default Helpers;
